export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/mysql/bin
export CBF_HOME=$WORKSPACE/Tools/build
export P4_HOME="/Users/alfasi/dev"
export BUILD_TOOLS="$P4_HOME/depot/Tools"
export PATH="/Users/alfasi/dev/CherryPy-3.2.4:$PATH"
export NETFLIX_ENVIRONMENT="test"
export M2_HOME=/user/apple/apache-maven-3.0.3
export M2=$M2_HOME/bin
export PATH=$M2:$PATH
export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/site-packages

source ~/.aliases
source ~/.profile

PATH=$PATH:/usr/local/bin/
#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
[[ -s "/Users/alfasi/.gvm/bin/gvm-init.sh" ]] && source "/Users/alfasi/.gvm/bin/gvm-init.sh"
