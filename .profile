
echo "
                   ,
                   |'.             ,
                   |  '-._        / )
                 .'  .._  ',     /_'-,
                '   /  _'.'_\   /._)')
               :   /  '_' '_'  /  _.'
               |E |   |Q| |Q| /   /
              .'  _\  '-' '-'    /
            .'--.(S     ,__, )  /
                  '-.     _.'  /
                __.--'----(   /
            _.-'     :   __\ /
           (      __.' :'  :Y
            '.   '._,  :   :|
              '.     ) :.__:|
                \    \______/
                 '._L/_H____]
                  /_        /
                 /  '-.__.-')
                :      /   /
                :     /   /
              ,/_____/----;
              '._____)----'
              /     /   /
             /     /   /
           .'     /    \\
          (______(-.____)

alfasin "

#source ~/gitcompletion.bash
#source ~/gitmarker.bash
cd ~/dev/
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_45.jdk/Contents/Home/
export CBF_HOME=/Users/alfasi/dev/cbf
export NEBULA_HOME=/Users/alfasi/dev/wrapper
export NETFLIX_ENVIRONMENT=local
export PATH=$PATH:/Users/alfasi/dev/nflx-corebot/root/apps/corebot/corebot:/Users/alfasi/dev/nflx-corebot/root/apps/corebot/corebot/plugins:/Users/alfasi/dev/cloudsol-python-libs/root/apps/nflx-cloudsol-python-libs:/Users/alfasi/dev/nflx-corebot/root/apps/corebot
export TOOLS_PARENT_DIR=/Users/alfasi/dev/depot

export NETFLIX_ENVIRONMENT=test
 

alias l='ls -lasrt'
alias hm='cd ~/dev/nflx-howlermonkey/root/apps/howler-monkey'
alias dev='cd ~/dev'
alias ll='ls -lta'
alias alfasin='rssh root@108.166.105.200'
alias python3='/usr/local/Cellar/python3/3.3.3/Frameworks/Python.framework/Versions/3.3/bin/python3.3'

function installnetflixcloudsol(){
    P4_HOME=/Users/alfasi/dev/depot
    cd $P4_HOME/cloud/rpms/nflx-cloudsol-python-libs/root/apps/nflx-cloudsol-python-libs
    python setup.py install
    cd $P4_HOME/cloud/rpms/nflx-cloudsol-python-libs/root/apps/cryptex/etc
    sudo mkdir -p /apps/cryptex/etc
    sudo cp *.pem /apps/cryptex/etc
}

alias meta='sudo ifconfig lo0 alias 169.254.169.254; sudo ipfw add 100 fwd 127.0.0.1,8080 tcp from any to any 80 in'

function _sync_to_bastion_service(){

    if [[ $# -ne 2 ]]; then
        echo " "
        echo "Usage: _sync_to_bastion_service bastion_to_sync_to folder"
        echo " "
    else

        if which md5sum; then
            ssh $(whoami)@$1 mkdir -p /appsvol/u/$(whoami)/$2/
            rsync -avze ssh ./ $(whoami)@$1:/appsvol/u/$(whoami)/$2/
            echo "Sleeping until new modifications are detected ..."
            while true; do
                CURRENTMD5=`find ./ -type f -exec md5sum {} + | awk '{print $1}' | sort | md5sum`
                if [[ "$CURRENTMD5" != "$PREVIOUSMD5" ]]; then
                    PREVIOUSMD5=`find ./ -type f -exec md5sum {} + | awk '{print $1}' | sort | md5sum`
                    echo "Modification detected ... syncing ..."
                    rsync -avze ssh ./ $(whoami)@$1:/appsvol/u/$(whoami)/$2/
                    echo "Syncing complete for $2 on $1 ... sleeping until new modifications are detected ... [$(date)]"
                fi
                sleep 1
            done
        else
            echo "Error: md5sum util not available. Please install using 'brew install coreutils'"
        fi
    fi
}

function sync_cdescripts_test(){
    cd /appsvol/u/$(whoami)/cdescripts/root
    _sync_to_bastion_service cdebastion-develop.test.netflix.net cdescripts/root   
}
function sync_cdescripts_prod(){
    cd /appsvol/u/$(whoami)/cdescripts/root
    _sync_to_bastion_service cdebastion-develop.prod.netflix.net cdescripts/root   
}

function sync_winston(){

    bastion='winstoncde-develop-euwest1.test.netflix.net'
    my_folder='/opt/stackstorm/packs/netflix_$(whoami)/'
    home='/Users/alfasi/dev/winston-cde-pack/root/opt/stackstorm/packs/netflix_winston/'

    echo "Making sure the folder exists, and create it if not..."
    /usr/bin/ssh $(whoami)@${bastion} "sudo mkdir -p ${my_folder}; sudo chown $(whoami):$(whoami) ${my_folder}"

    echo "Starting the sync loop..."
    if which md5sum; then
        /usr/bin/ssh $(whoami)@${bastion} mkdir -p ${my_folder}/
        rsync -avz --exclude='.idea/' --exclude='.git/' -e ssh ${home} $(whoami)@${bastion}:${my_folder}/
    else
        echo "Error: md5sum util not available. Please install using 'brew install coreutils'"
    fi
    
}
export MITSCHEME_LIBRARY_PATH="/Applications/MIT\:GNU\ Scheme.app/Contents/Resources"
export MITSCHEME_LIBRARY_PATH="/Applications/MIT\:GNU\ Scheme.app/Contents/Resources"
export CATALINA_HOME="/Library/Tomcat"
export MIT_SCHEME_EXE="/usr/local/scheme"
export BASTION_ENVIRONMENT="test"
export EC2_LOCAL_IPV4="127.0.0.1"
